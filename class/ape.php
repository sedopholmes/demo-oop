<?php

require_once ('animal.php');

    class Ape extends Animal
    {
        private $voice = "Auooo";

        public function yell()
        {
            echo "Animal name: " . $this->name . "<br>";
            echo "Animal voice: " . $this->voice . "<br>";
            echo "Total legs: " . $this->legs . "<br>";
            echo "Cold blood type: " . $this->cold_blooded . "<br>";
        }
    }