<?php

require_once ('animal.php');
    
    class Frog extends Animal
    {
        private $move = "hop hop";
        public $legs = "4";
        public $cold_blooded = "true";

        public function jump()
        {
            echo "Animal name: " . $this->name . "<br>";
            echo "Animal move: " . $this->move . "<br>";
            echo "Total legs: " . $this->legs . "<br>";
            echo "Cold blood type: " . $this->cold_blooded;
        }
    }