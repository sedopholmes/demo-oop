<?php
    
require ('class/ape.php');
require ('class/frog.php');

    echo "<h3>Release 0</h3>";

    $sheep = new Animal("shaun");
    echo "Animal name: " . $sheep->name . "<br>"; // "shaun"
    echo "Total legs: " . $sheep->legs . "<br>"; // 2
    echo "Cold blood type: " . $sheep->cold_blooded . "<br>"; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    echo "<h3>Release 1</h3>";
    
    $sungokong = new Ape("kera sakti");
    $sungokong->name ."<br>";
    $sungokong->legs ."<br>";
    $sungokong->yell(); // "Auooo"
    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->name . "<br>";
    $kodok->legs . "<br>";
    $kodok->jump(); // "hop hop"

?>